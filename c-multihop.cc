#include "ns3/netanim-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/animation-interface.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

using namespace ns3;

int NUM_NODES = 50;
int NETWORK_X = 100;
int NETWORK_Y = 100;

// the percentage of the nodes in the
// network that would ideally be cluster
// heads during any one round of the
// simulation, default is 0.05
double CLUSTER_PERCENT = 0.05;

// the total rounds that the simulation
// should run for - the network lifetime
// default is 2000
int TOTAL_ROUNDS = 2000;

int BASE_STATION_X_DEFAULT = 102;

int BASE_STATION_Y_DEFAULT = 103;

int DEAD_NODE = 1;
int MESSAGE_LENGTH = 8;

int TRIALS = 15;

struct sensor
{
  short xLoc;
  short yLoc;
  short lPeriods;
  short ePeriods;
  double pAverage;
  int round;
  int head; //-1 for CH
  int cluster_members;
  int head_count;
  double range;
  int neighbor[50];
};

struct sensor BASE_STATION;

void initializeNetwork (struct sensor network[]);
void scanRange (struct sensor nodes[]);
void checkHop (struct sensor nodes[]);

struct sensor *loadConfiguration (char *filename);

int runmultihopSimulation (const struct sensor network[]);

int
main (int argc, char *argv[])
{
  struct sensor *network;

  // int i = 0;
  //int j = 0;
  // int rounds_multihop = 0;
  int found = false;
  //double average_comparison = 0.0;
  char *filename = new char[10];

  BASE_STATION.xLoc = BASE_STATION_X_DEFAULT;
  BASE_STATION.yLoc = BASE_STATION_Y_DEFAULT;

  // search command line arguments for the -f switch
  // if the -f switch is set, the next argument will be
  // the configuration file name to use
  /**for(i = 0; i  <= argc; i++){  
        if((argv[i][0] == '-') && (argv[i][1] == 'f')){  
            found = i;  
        }  
    } **/
  found = true;
  // the search for -f was unsuccessful and therefore the
  // the default configuration file name should be used
  if (found == false)
    {
      strcpy (filename, ".config");
      network = loadConfiguration ((char *) ".config");
      // else the search was successful and the file name is
      // located at one position in the argument array past
      // where the flag was found
    }
  else
    {
      network = loadConfiguration (argv[found + 1]);
    }
  initializeNetwork (network);
  scanRange (network);
  checkHop (network);
  return 0;
}

int
runmultihopSimulation (const struct sensor network[])
{
  struct sensor *network_multihop; // wireless sensor network to run sim on

  int i = 0; // indexing variables
  int j = 0;
  int k = 0;
  int closest = 0;

  int round = 0; // current round

  double distance_X_old = 0.0;
  double distance_Y_old = 0.0;
  double distance_old = 0.0;
  double distance_X_new = 0.0;
  double distance_Y_new = 0.0;
  double distance_new = 0.0;
  // int recent_round = 1 / CLUSTER_PERCENT;
  double threshold = CLUSTER_PERCENT / 1 - CLUSTER_PERCENT * (round % 1 / CLUSTER_PERCENT);
  double random_number;
  int cluster_head_count = 0;
  double percent_found = 0.0;
  network_multihop = (struct sensor *) malloc (NUM_NODES * sizeof (struct sensor));

  // copy the contents of the passed network to a temporary
  // network so the same network can be passed to different
  // protocol simulations

  for (i = 0; i <= NUM_NODES; i++)
    {
      network_multihop[i].xLoc = network[i].xLoc;
      network_multihop[i].cluster_members = 0;
      network_multihop[i].yLoc = network[i].yLoc;
      network_multihop[i].ePeriods = network[i].ePeriods;
      network_multihop[i].pAverage = network[i].pAverage;
    }

  for (j = 0; j <= TOTAL_ROUNDS; j++)
    {
      // here we recalculate all the variables
      // which are round dependent
      threshold = CLUSTER_PERCENT / 1 - CLUSTER_PERCENT * (round % 1 / CLUSTER_PERCENT);
      cluster_head_count = 0;

      // advertisement phase
      // we determine which nodes will be cluster heads
      for (i = 0; i <= NUM_NODES; i++)
        {
          // if (network_multihop[i].round < (j - recent_round) || (j - recent_round == 0))
          //   {
          if (network_multihop[i].head != DEAD_NODE)
            {
              random_number = .00001 * (rand () % 100000);
              if (random_number <= threshold)
                {
                  // the random number selected is less
                  // than the threshold so the node becomes
                  // a cluster head for the round
                  network_multihop[i].head_count++;
                  // update the round variable
                  // so we know that this sensor was
                  // last a cluster head at round i
                  network_multihop[i].round = j;
                  network_multihop[i].head = -1;
                  // store the index of the node in the
                  // cluster_heads array
                  // increment the cluster_head_count
                  cluster_head_count++;
                }
            }
          // }
        }

      percent_found += (double) cluster_head_count / (double) NUM_NODES;

      // CLUSTER SET-UP PHASE
      // for this phase, all non-cluster heads determine to which
      // cluster they will broadcast and alert the cluster head to that

      for (i = 0; i <= NUM_NODES; i++)
        {
          closest = -1;
          if ((network_multihop[i].head != -1) && network_multihop[i].head != DEAD_NODE)
            {
              // if the node's round is not equal to the
              // current round, the node is not a cluster
              // head and we must find a cluster head for
              // the node to transmit to
              for (k = 0; k <= NUM_NODES; k++)
                {
                  if (network_multihop[k].head == -1 && closest != -1)
                    {
                      distance_X_old = network_multihop[i].xLoc - network_multihop[closest].xLoc;
                      distance_Y_old = network_multihop[i].yLoc - network_multihop[closest].yLoc;
                      distance_old = sqrt (pow (distance_X_old, 2) + pow (distance_Y_old, 2));
                      distance_X_new = network_multihop[i].xLoc - network_multihop[k].xLoc;
                      distance_Y_new = network_multihop[i].yLoc - network_multihop[k].yLoc;
                      distance_new = sqrt (pow (distance_X_new, 2) + pow (distance_Y_new, 2));
                      if (distance_new < distance_old)
                        closest = k;
                    }
                  else if (network_multihop[k].head == -1 && closest == -1)
                    {
                      closest = k;
                    }
                }

              network_multihop[i].head = closest;
              network_multihop[closest].cluster_members++;
            }
        }

      // round has completed, increment the round count
      for (i = 0; i <= NUM_NODES; i++)
        {
          network_multihop[i].cluster_members = 0;
          network_multihop[i].head = 0;
        }

      cluster_head_count = 0;
      round++;
    }

  free (network_multihop);
  return round;
} // end runLeachSimulation function

struct sensor *
loadConfiguration (char *filename)
{
  int i = 0;
  int axis = 0;

  char buf[80]; // buffer for reading lines from file
  char copy[80]; // temporary memory for storing the
  char cut[8]; // integer result of a line
  char cut2[8];

  FILE *fp;
  // open file with settings as read only,
  // if file cannot be opened, allocate the memory
  // based on the default values and return
  if ((fp = fopen (filename, "r")) == NULL)
    {
      return (struct sensor *) malloc (NUM_NODES * sizeof (struct sensor));
    }

  // read entire configuration file
  while (fgets (buf, 80, fp) != NULL)
    {
      // if the line is the NUM_NODES variable
      strncpy (cut, buf, 7);
      strncpy (cut2, cut, 7);
      if (cut[0] == '#')
        {
          i = 0;
        }
      else if (cut[0] == 'N' && cut[1] == 'U')
        {
          i = 9;
          // advance pointer to = sign
          while (buf[i] != '=')
            {
              i++;
            }
          // then advance pointer one more space
          // past the equal sign
          i++;
          // if there is any whitespace, advance
          // pointer past it
          while (buf[i] == ' ')
            {
              i++;
            }
          // copy the contents of the line read from the file
          // into the copy variable from the iterator i
          strcpy (copy, buf + i);
          // convert the copied characters to NUM_NODES
          NUM_NODES = atoi (copy);
        }

      else if (cut[0] == 'N' && cut[1] == 'E')
        {
          i = 6;
          // advance pointer to = sign
          while (buf[i] != '=')
            {
              if (buf[i] == 'X')
                axis = 1;
              if (buf[i] == 'Y')
                axis = 0;
              i++;
            }
          // then advance pointer one more space
          // past the equal sign
          i++;
          // if there is any whitespace, advance
          // pointer past it
          while (buf[i] == ' ')
            {
              i++;
            }
          // copy the contents of the line read from the file
          // into the copy variable from the iterator i
          strcpy (copy, buf + i);
          if (axis == 1)
            NETWORK_X = atoi (copy);
          if (axis == 0)
            NETWORK_Y = atoi (copy);
        }
      else if (cut[0] == 'R' && cut[2] == 'U' && cut[4] == 'D')
        {
          i = 6;
          // advance pointer
          while (buf[i] != '=')
            {
              i++;
            }
          // then advance pointer one more space
          // past the equal sign
          i++;
          // if there is any whitespace, advance
          // pointer past it
          while (buf[i] == ' ')
            {
              i++;
            }
          // copy the contents of the line read from the file
          // into the copy variables from the iterator i
          strcpy (copy, buf + i);
          // convert the copied characters to an integer
          // and store it in total rounds
          TOTAL_ROUNDS = atoi (copy);
        }
    }

  if (fclose (fp) != 0)
    {
      printf ("Error closing config file.\n");
      exit (1);
    }

  return (struct sensor *) malloc (NUM_NODES * sizeof (struct sensor));

} // end loadConfiguration function

void
initializeNetwork (struct sensor network[])
{
  int i = 0;
  srand ((unsigned int) time (0));

  for (i = 0; i < NUM_NODES; i++)
    {
      network[i].xLoc = rand () % NETWORK_X;
      network[i].yLoc = rand () % NETWORK_Y;
      network[i].lPeriods = 0;
      network[i].ePeriods = TOTAL_ROUNDS;
      network[i].pAverage = 0.00;
      network[i].round = false;
      network[i].head = false;
      network[i].range = 20.0;
    }

} // end initializeNetwork function

void
scanRange (struct sensor nodes[])
{
  int i, j = 0;
  double distance_X = 0.0;
  double distance_Y = 0.0;
  double distance = 0.0;

  for (i = 0; i < NUM_NODES; i++)
    {
      for (j = 0; j < NUM_NODES; j++)
        {
          distance = 0;
          distance_X = nodes[i].xLoc - nodes[j].xLoc;
          distance_Y = nodes[i].yLoc - nodes[j].yLoc;
          distance = sqrt (pow (distance_X, 2) + pow (distance_Y, 2));
          if (distance <= nodes[i].range)
            {
              nodes[i].neighbor[j] = 1;
              // printf ("node %d is neighbour of node %d\n", j, i);
            }
          else
            {
              nodes[i].neighbor[j] = 0;
            }
        }
    }
}

void
checkHop (struct sensor nodes[])
{
  int i, j, count = 0;

  for (i = 0; i < NUM_NODES; i++)
    {
      count = 0;
      for (j = 0; j < NUM_NODES; j++)
        {
          if (nodes[i].neighbor[j] == 0)
            {
              
              count++;
            }
        }
    }
}